package DaySix;


public class Car extends Vehicle implements GearBox,Engine {

    private String name;

    public Car(String name){
        this.name = name;
    }
    @Override
    public void hasValve() {
        System.out.println("The car has 6 valves");
    }

    @Override
    public void hasNumberOfGear() {
        System.out.println("The car has 4 gears");

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void needGas() {
        System.out.println("This car needs gas");
    }
}
