package Week3_List;

import DayFive.Employee;

import java.util.ArrayList;
import java.util.List;

/**
 * ArrayList sample below
 */
public class ListSample {
    ArrayList <String> strArray = new ArrayList<>();
    ArrayList <String> newStrArray = new ArrayList<>();
    ArrayList <Employee> employees = new ArrayList<>();

    /**
     * ArrayList of String
     */
    public void addToTheList(){
        int i = 0;
        while(i < strArray.size()){
            strArray.add("List");
            i++;
        }

    }

    /**
     * ArrayList of Objects
     */
    public void addEmployees(){
        Employee em1 = new Employee(2000, "Feebi");
        Employee em2 = new Employee(3000, "Harry");
        Employee em3 = new Employee(5000, "Lisa");

        employees.add(em1);
        employees.add(em2);
        employees.add(em3);
    }

    /**
     * AraryList of String and do while
     */
    public void getAll(){
        int i = 0;
        do{
            strArray.add("Get Strings");
            i++;
        }
        while(i < strArray.size());

    }
}
