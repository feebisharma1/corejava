package DayTwo;


public class DayTwo {
/*
    private int num1 = 10;
    private int num2 = 20;
    private boolean isDayTwo = true;
*/
    //constructor
    public DayTwo(int x, String y){

    }

    public void displayText(){
        System.out.println("Sample Text");
    }

    public void displayText(String Text){
        System.out.println(Text);
    }

    //ifelse
    public void comparison(int num){
        if(num > 10){
            System.out.println(num+ " is greater than 10");
        }
        else{
            System.out.println(num+ " is less than 10");
        }
    }

    public void checkSession(boolean isDayTwo){
        if(isDayTwo){
            System.out.println("today is day two");
        }
        else{
            System.out.println("Today is not a day two");
        }
    }
}
