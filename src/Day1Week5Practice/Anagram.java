package Day1Week5Practice;

import java.util.Arrays;

public class Anagram {
    public void checkWord(){
        String word1 = "allergy";
        String word2 = "gallery";

        /**
         *  if length varies then it is not anagram so
         */
        if(word1.length() != word2.length()){
            System.out.println("Not an anagram");
        }

        /**
         * Using sorting method
         */
        char[] w1 = word1.toCharArray();
        Arrays.sort(w1);
        char[] w2 = word2.toCharArray();
        Arrays.sort(w2);
        if(Arrays.equals(w1, w2)){
            System.out.println("It is Anagram");
        }
        else{
            System.out.println("It is not an Anagram");
        }

    }
}
