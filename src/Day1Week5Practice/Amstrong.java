package Day1Week5Practice;

/**
 * • Check if the given number is Armstrong number or not.
 * • Sample 0, 1, 153, 370, 371 , 407
 * E.g
 * 370
 * = 3^3 + 7^3_1^3
 * = 27 + 343 + 1
 * = 371
 */
public class Amstrong {
    public void checkNum(int checkNumber){
        //int temp = 371;
        //int  num = 371;
        int num = checkNumber;
        int temp = 0;
        int power = 0;

        while(num >= 1) {
            temp = num % 10;
            num = num / 10;
            power = power + (int) (Math.pow(temp, 3));
        }
        System.out.println(power);
        if(power == checkNumber){
            System.out.println(power);
        }
        else{
            System.out.println("Its not correct");
        }

    }
}
