package WeekFourPractice;

import java.util.Arrays;

/**
 * Given sample of string/char array is as follow {z,d,g,k,h,e,,o,j,l,a,c,x} Find the e using bubble sort
 * • Steps: arrange in proper order
 * • Divide and search
 */
public class Sorting {
    public void doSort(){
        char temp = ' ';
        char[] charArr = {'z','d','g','k', 'h','e',' ','o','j','l','a','c','x'};
        for(int i = 0; i < charArr.length; i++) {
            for (int j = 0; j < charArr.length-1; j++) {
                if(charArr[j] > charArr[j+1]){
                    temp = charArr[j];
                    charArr[j] = charArr[j+1];
                    charArr[j+1] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(charArr));

    }
}
