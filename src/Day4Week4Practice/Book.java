package Day4Week4Practice;

/**
 * Parent class
 */
public class Book {
    private String Name;
    private String Author;
    private String Date;
    private String Publisher;

    public Book(String name, String author, String date, String publisher) {
        Name = name;
        Author = author;
        Date = date;
        Publisher = publisher;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getPublisher() {
        return Publisher;
    }

    public void setPublisher(String publisher) {
        Publisher = publisher;
    }
}
