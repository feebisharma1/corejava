package Day4Week4Practice;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Main Class
 */
public class Store {

    ArrayList<Novel> novelList = new ArrayList<>();
    ArrayList<EducationBook> eduList = new ArrayList<>();

    public void addNovel(){
        Novel nv1 = new Novel("The Great Gatsby", "Charles", "12/14/2010", "Charles", "Romantic", "Teaser","10/06/2012", 4, 340);
        Novel nv2 = new Novel("The Last Summer", "Nicholas", "12/14/2010", "Sparks", "Romantic", "Teaser","10/06/2012", 4, 340);
        novelList.add(nv1);
        novelList.add(nv2);
        displayNovel();
    }

    public void addEduBooks(){
        EducationBook eduBook = new EducationBook("Science", "Einstien", "1997", "ALbert", 5, "School", 4, 450);
        eduList.add(eduBook);
        displayEduBooks();

    }

    public void displayNovel(){
        for(Novel objN: novelList){
            System.out.println(objN.getName());
        }
    }

    public void displayEduBooks(){
        for(EducationBook objE: eduList){
            System.out.println(objE.getName() + " " + objE.getEdition());
        }
    }

}
