package Day4Week4Practice;

/**
 * Child Class
 */
public class EducationBook extends Book {
    private int courseLevel;
    private String affiliatedFaculty;
    private int edition;
    private int price;

    public EducationBook(String name, String author, String date, String publisher, int courseLevel, String affiliatedFaculty, int edition, int price) {
        super(name, author, date, publisher);
        this.courseLevel = courseLevel;
        this.affiliatedFaculty = affiliatedFaculty;
        this.edition = edition;
        this.price = price;
    }

    public int getCourseLevel() {
        return courseLevel;
    }

    public void setCourseLevel(int courseLevel) {
        this.courseLevel = courseLevel;
    }

    public String getAffiliatedFaculty() {
        return affiliatedFaculty;
    }

    public void setAffiliatedFaculty(String affiliatedFaculty) {
        this.affiliatedFaculty = affiliatedFaculty;
    }

    public int getEdition() {
        return edition;
    }

    public void setEdition(int edition) {
        this.edition = edition;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
