package Day4Week4Practice;

/**
 * Child class
 */
public class Novel extends Book {

    private String genre;
    private String subGenre;
    private String reDate;
    private int edition;
    private int price;

    public Novel(String name, String author, String date, String publisher, String genre, String subGenre, String reDate, int edition, int price) {
        super(name, author, date, publisher);
        this.genre = genre;
        this.subGenre = subGenre;
        this.reDate = reDate;
        this.edition = edition;
        this.price = price;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getSubGenre() {
        return subGenre;
    }

    public void setSubGenre(String subGenre) {
        this.subGenre = subGenre;
    }

    public String getReDate() {
        return reDate;
    }

    public void setReDate(String reDate) {
        this.reDate = reDate;
    }

    public int getEdition() {
        return edition;
    }

    public void setEdition(int edition) {
        this.edition = edition;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
