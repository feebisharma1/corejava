package DayFive;

/**
 * Super Class
 */
public class Employee {
    private String firstName;
    private String lastName;

    public Employee(int i, String feebi) {
    }


    public double annualSalary(double salary){
        return (salary * 12);
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void leaveDays(){
        System.out.println("There are 30 days leaving period for all employee");
    }
}
