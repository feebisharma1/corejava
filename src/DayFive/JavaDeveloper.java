package DayFive;

/**
 * child class
 */
public class JavaDeveloper extends Employee {
    public JavaDeveloper(int i, String feebi) {
        super(i, feebi);
    }

    public void devRoles(){
        System.out.println("Writing Java Codes");
    }

    @Override
    public void leaveDays() {
        super.leaveDays();
    }

    @Override
    public double annualSalary(double salary) {
        return (salary * 2 + (salary/2));
    }
}
