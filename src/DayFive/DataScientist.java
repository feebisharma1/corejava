package DayFive;

/**
 * child class
 */
public class DataScientist extends Employee{

    public DataScientist(int i, String feebi) {
        super(i, feebi);
    }

    @Override
    public double annualSalary(double salary) {
        return super.annualSalary(salary);
    }

    @Override
    public void leaveDays() {
        System.out.println("There are 35 leave days");
    }
}
