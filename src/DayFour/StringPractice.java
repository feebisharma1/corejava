package DayFour;

/**
 * Practising some String Manipulation
 * Create a Java class with name “StringPractice”
 * Join two String variables (stringA,stringB)
 * Get a character from stringA variable at position 3
 * Convert variable stringA to upperCase
 * Convert variable stringB to lowerCase
 * Check if the string variables ends with “er”
 */
public class StringPractice {
    //instance variable
    private String stringA = "Tower";
    private String stringB = "JAVA";


    /**
     * concatination
     */
    public void joinString() {
        System.out.println(stringA.concat(stringB));
    }

    /**
     * finding value at any position*/
    public void getStringByIndex() {
        int position = 3;
        System.out.println(stringA.charAt(3));
    }

    /*to Upper Case*/
    public void convertToUpperCase() {
        System.out.println(stringA.toUpperCase());
    }

    //to LowerCase
    public void convertToLowerCase() {
        System.out.println(stringB.toLowerCase());
    }

    //Checking the variable
    public void checkVariables() {
        if (stringA.endsWith("er")) {
            System.out.println("Yes, it ends with er");
        }

        if (stringB.endsWith("er")) {
            System.out.println("Yes, it ends with er");
        }
    }
}
