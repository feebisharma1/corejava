package DayFour;

import java.util.HashMap;

/**
 * Practising basic Hashmap functions
 * Create a class called HashMap sample
 * Create a local hashmap
 * Add key and value to hashmap (atleast 3)
 * Create a method called displayValue
 * Using for each loop to display the value using
 * hasmap.value() method
 * Sample
 * for(Integer x:hashMap){
 * <p>
 * }
 */
public class HashmapSample {

    public void displayValue() {
        HashMap<Integer, String> hm = new HashMap<Integer, String>();
        hm.put(1, "One");
        hm.put(2, "Two");
        hm.put(3, "Three");

        for (Integer x : hm.keySet()) {
            System.out.println(hm.get(x));
        }
    }

}
