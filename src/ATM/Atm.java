package ATM;

import java.util.Scanner;

/**
 * Create a program called ATM with following features
 * • initial balance should be 0
 * • refillBalance method that accepts amount till 100
 * • withdrawCash() which provides the withdrawal request based upon available balance
 * • fastCash() method to withdraw amount like 5 , 10 ,20 50
 * • if the balance less than 5 , display the message to refill soon
 * • exit() method to leave ATM with “Thank You! “ message
 */
public class Atm {
    private double initalBalance = 0.00;

    public void refillBalance(){
        System.out.println("********Refil Balance**********");
        Scanner scanner  = new Scanner(System.in);
        System.out.print("Please enter the amount to be deposited: ");
        double deposit = scanner.nextDouble();
        if(deposit <= 100){
            initalBalance = initalBalance+deposit;
        }
        else{
            System.out.println("Please deposit amount less than 100");
        }
        System.out.println("Your initial balance is " +initalBalance);
    }

    public void withdrawCash(){
        System.out.println("**********Withdraw Cash***********");
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please enter the amount to be withdrawn: ");
        double withdraw = scanner.nextDouble();
        if(initalBalance > withdraw){
            initalBalance = initalBalance-withdraw;
        }
        else if(initalBalance < withdraw){
            System.out.println("Cannot be withdrawn");
        }
        else{
            System.out.println("You have no balance in the account");
        }
        System.out.println("Your remaining balance is "+ initalBalance);
    }

    public void fastCash(){
        System.out.println("************Fast Cash***********");
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please enter the amount to be fast cashed: ");
        double fastCash = scanner.nextDouble();
        if((initalBalance >= 5) && (fastCash % 5 == 0)){
            initalBalance = initalBalance - fastCash;
            System.out.println("Your remaining balance is "+initalBalance);
        }
        else if(fastCash % 5 != 0){
            System.out.println("Please enter the amount that is multiple of 5's");
        }
        else{
            System.out.println("Please refill the balance on your account.");
        }
    }

    public void exit(){
        System.out.println("Thank you valued customer!!");
    }

    public void displayAll(){
        System.out.println("Welcome to ATM");
        System.out.println("");
    }


}
