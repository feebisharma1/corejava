package DayEight_Poly;

/**
 * Encapsulation
 */
public class User {
    private int id;
    public String fullName;
    private String address;
    private String email;
    private String phoneNumber;
    private char gender;

    public User(){

    }

    public User(int id, String fullName, String address, String email, String phoneNumber, char gender) {
        this.id = id;
        this.fullName = fullName;
        this.address = address;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.gender = gender;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        if(validateEmail()) {
            this.email = email;
        }
        else{
            System.out.println("NOt a valid email");
        }
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    private boolean validateEmail(){
        return true;
    }
}
