package SampleThread;

public class SThread extends Thread{
    private boolean flag = true;

    @Override
    public void run() {
        try{
            while(flag) {
                System.out.println("Thread is running");
                sleep(5000);
                System.out.println("Second thread is running");
                kill();
            }
        }
        catch (InterruptedException e){
            e.printStackTrace();
        }
        System.out.println("Stopped thread");
    }

    private void kill() {
        flag = false;
    }


    @Override
    public synchronized void start() {
        super.start();
    }


}
