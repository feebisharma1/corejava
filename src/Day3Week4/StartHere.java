package Day3Week4;

import java.util.Scanner;

public class StartHere {
    public void displayMenu(){
        System.out.println("**** MENU ****");
        System.out.println("1: Food");
        System.out.println("2: Drink");
        System.out.print("Please choose one option from above: ");
        Scanner scanner = new Scanner(System.in);
        int input = scanner.nextInt();
        if(input == 1){
            MainMenu menu = new MainMenu("Food");
            orderFood();

        }
        else if(input == 2){
            MainMenu menu = new MainMenu("Drink");
            orderDrinks();
        }
        else{
            System.out.println("Please enter a valid number");
        }
    }

    public void orderFood(){
        System.out.println("**** FOOD ****");
        System.out.println("1: Croissant");
        System.out.println("2: Chocolate Donut");
        System.out.println("3: Bagel");
        System.out.println("4: Butter Toast");
        System.out.println("5: Apple Pie");
        System.out.println("6: None");
        System.out.print("Please choose one of the option above: ");
        Scanner scanner =  new Scanner(System.in);
        int input = scanner.nextInt();

        if(input == 1){
            Food food = new Food("Croissant", 2);
            printFoodOrdersAndPrice(food.getFoodName(),food.getFoodPrice());
        }
        else if(input == 2){
            Food food = new Food("Chocolate Donut", 3);
            printFoodOrdersAndPrice(food.getFoodName(),food.getFoodPrice());
        }
        else if(input == 3){
            Food food = new Food("Bagel", 4);
            printFoodOrdersAndPrice(food.getFoodName(),food.getFoodPrice());
        }
        else if(input == 4){
            Food food = new Food("Butter Toast", 1);
            printFoodOrdersAndPrice(food.getFoodName(),food.getFoodPrice());
        }
        else if(input == 5){
            Food food = new Food("Apple Pie", 5);
            printFoodOrdersAndPrice(food.getFoodName(),food.getFoodPrice());
        }
        else if(input == 6){
            Food food = new Food("None", 0);
            printFoodOrdersAndPrice(food.getFoodName(),food.getFoodPrice());
        }
        else{
            System.out.println("Please enter a valid number.");
        }
    }

    public void printFoodOrdersAndPrice(String food, int price){
        System.out.println("Your order is " + food + " of costs $"+ price );
    }

    public void orderDrinks(){
        System.out.println("****** DRINKS ******");
        System.out.println("1: Tea");
        System.out.println("2: Coffee");
        System.out.println("3: Milkshake");
        System.out.println("4: None");
        System.out.println("Please choose one of the above: ");
        Scanner scanner = new Scanner(System.in);
        int input = scanner.nextInt();

        if (input == 1){
            Drink drink = new Drink("Tea", 4);
            printDrinksNameandPrice(drink.getDrinkName(), drink.getDrinkPrice());

        }
        else if(input == 2){
            Drink drink = new Drink("Coffee", 3);
            printDrinksNameandPrice(drink.getDrinkName(), drink.getDrinkPrice());

        }
        else if(input == 3){
            Drink drink = new Drink("Milkshake", 6);
            printDrinksNameandPrice(drink.getDrinkName(), drink.getDrinkPrice());

        }
        else if(input == 4){
            Drink drink = new Drink("None", 0);
            printDrinksNameandPrice(drink.getDrinkName(), drink.getDrinkPrice());

        }
        else{
            System.out.println("Please enter a valid number");

        }
    }

    public void printDrinksNameandPrice(String drink, int price){
        System.out.println("Your Drink is "+ drink+" of cost "+ price);
    }
}
