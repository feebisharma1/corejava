package Day3Week4;

public class MainMenu {
    private String Name;

    public MainMenu(String name) {
        Name = name;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
