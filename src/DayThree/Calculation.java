package DayThree;

public class Calculation {
    public int intSum(int a, int b, int c){
        int sum = a + b + c;
        return sum;
    }

    public void intSub(int a, int b){
        int sub = 0;
        if(a > b) {
            sub = a - b;
        }
        if (b > a){
            sub = b - a;
        }
        System.out.println("Subtraction: " +sub);
    }

    public void intMul(int a, int b){
        int mul = a * b;
        System.out.println("Multiplication: " +mul);
    }

    public void intDiv(int a, int b){
        int div = 0;
        if(a > b){
            div = a/b;
        }
        if(b > a){
            div = b /a;
        }
        System.out.println("Division: " +div);
    }

}
