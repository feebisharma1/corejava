package DayThree;

public class LogicGate {
    private boolean gateIsOpen = true;
    private boolean roomIsOpen = false;

    public void checkGate(){
        if(gateIsOpen){
            System.out.println("The gate is Open");
        }
        else{
            System.out.println("The gate is Closed");
        }
    }

    public void checkRoom(){
        if(gateIsOpen && roomIsOpen){
            System.out.println("I am in the room");
        }
        if(!gateIsOpen || !roomIsOpen){
            System.out.println("I am not in the room");
        }
    }
}
