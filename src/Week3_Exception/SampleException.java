package Week3_Exception;

import java.util.ArrayList;

public class SampleException {
    public void testNullPointerException(){
        try{
            String text= null;
            System.out.println(text.length());
        }
        catch (NullPointerException e){
            System.out.println("String should not be empty");
        }
        finally {
            System.out.println("please insert string with value");
        }

    }

    public void testArithmeticException(){
        try{
            int num1 = 10;
            int num2 = 0;
            int div = (num1/num2);
            System.out.println(div);

        }
        catch(ArithmeticException a){
            throw new ArithmeticException("ttype some numbers");
        }

    }

    public void testNumberFormatException() throws NumberFormatException{
        String num1 = "123";
        String num2 = "abc";

        System.out.println(Integer.parseInt(num1));
        System.out.println(Integer.parseInt(num2));
    }

    public void testArrayIndexOutOfBoundException(){
        char [] arr1 = {'a', 'e', 'i', 'o', 'u'};
        System.out.println(arr1[4]);
        System.out.println(arr1[5]);
    }

    public void testIndexOutOfBoundsException(){
        ArrayList<String> arrList = new ArrayList<>();
        arrList.add("feebi");
        arrList.add("sharma");
        System.out.println(arrList.get(1));
        System.out.println(arrList.get(4));

    }



}
