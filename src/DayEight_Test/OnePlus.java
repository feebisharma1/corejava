package DayEight_Test;

/**
 *  a class called  OnePlus
 * and inherit the Phone class  and OnTouchScreenListener
 */
public class OnePlus extends MobilePhone implements OnTouchScreenListener {
    @Override
    public void getCarrierName() {

    }

    @Override
    public void hasDialPad() {

    }

    @Override
    public void sendMsg() {

    }

    @Override
    public void onTouch() {

    }

    @Override
    public void onSwipe() {

    }

    @Override
    public void onDrag() {

    }

    @Override
    public void onLongHold() {

    }
}
