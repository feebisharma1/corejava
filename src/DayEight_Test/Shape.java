package DayEight_Test;

/**
 * Create a  class called Shape with
 * a method called getHeight()
 * a method called getWidth()
 * a method called getRadius()
 * a method called getArea()
 * Create a class called Square which extends the Shape
 * Create a class called Rectangle which extends the Shape
 * Create a class called Circle which extends the Shape
 */

/**
 * This is a super class
 */
public class Shape {
    public void getHeight(int x){
        System.out.println(x);
    }

    public void getWidth(int y){
        System.out.println(y);
    }

    public void getRadius(int z){
        System.out.println(z);
    }

    public void getArea(int x, int y){
        int num = x * y;
        System.out.println(num);
    }
}
