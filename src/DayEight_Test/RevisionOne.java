package DayEight_Test;

/**
 * Create a class called RevisionOne which should have
 * two instance variables (1 int and 1 String)
 * an Empty constructor
 * another constructor with an integer and String variable
 * a public method with no return type
 * a public method with no return type but have two parameters (integer and String)
 * a public method with integer return type
 * a private method with String return type
 * a private method with String return type and have a parameter (String)
 */
public class RevisionOne {

    /**
     * Two instance variable
     */
    public int num;
    public String Vehicle;

    /**
     * Empty Constructor
     */
    public RevisionOne(){

    }

    /**
     * Parameterized Constructor
     */
    public RevisionOne(int num, String Vehicle){
        this.num = num;
        this.Vehicle = Vehicle;
    }

    /**
     * public method with no return type
     */
    public void seeVehicle(){
        System.out.println("I can see many vehicle in the road");
    }

    /**
     * a public method with no return type but have two parameters (integer and String)
     */
    public void typeVehicle(int x, String Vehicle){
        System.out.println("I can see " + x + " number of " + Vehicle);
    }

    /**
     *  a public method with integer return type
     */
    public int myVehicleNum(){
        return num;
    }

    /**
     * a private method with String return type
     */
    private String myVehicleType(){
        return Vehicle;
    }

    /**
     * a private method with String return type and have a parameter (String)
     */
    private String bestVehicle(String car){
        return car;
    }
}
