package DayEight_Test;

/**
 * Create a abstract class called Mobile Phone which have
 * -abstract method called getCarrierName();
 * -abstract method called hasDialPad();
 * -abstract method called sendMsg();
 * -method called hasWire();
 * -method called hasCamera();
 */
public abstract class MobilePhone {
    public abstract void getCarrierName();

    public abstract void hasDialPad();

    public abstract void sendMsg();

    /**
     * Normal method
     */
    public void hasWire(){

    }

    /**
     * Normal method
     */
    public void hasCamera(){

    }
}
