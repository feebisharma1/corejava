package DayEight_Test;

/**
 * a class called Pixel
 * and inherit the Phone
 * and OnTouchScreenListener
 */
public class Pixel extends MobilePhone implements OnTouchScreenListener{

    @Override
    public void onTouch() {

    }

    @Override
    public void onSwipe() {

    }

    @Override
    public void onDrag() {

    }

    @Override
    public void onLongHold() {

    }

    @Override
    public void getCarrierName() {

    }

    @Override
    public void hasDialPad() {

    }

    @Override
    public void sendMsg() {

    }
}
