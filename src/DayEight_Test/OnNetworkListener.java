package DayEight_Test;

/**
 * Create an interface called OnNetworkListener with methods
 * viewNetworkStrength();
 * onCall;
 * OnNetworkType()
 */
public interface OnNetworkListener {
    void viewNetworkStrength();

    void onCall();

    void OnNetworkType();
}
