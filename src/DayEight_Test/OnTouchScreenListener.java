package DayEight_Test;

/**
 * Create an interface called OnTouchScreenListener with methods
 * onTouch()
 * onSwipe()
 * onDrag()
 * onLongHold()
 */
public interface OnTouchScreenListener {
    void onTouch();

    void onSwipe();

    void onDrag();

    void onLongHold();
}
