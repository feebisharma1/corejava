package DayEight_Test;

/**
 * Create a class name Weeks
 *  * create a method which accepts number 1to 7
 *  * use switch under this method
 *  * based upon 1 to 7 display Sunday, Monday and So on
 */
public class Weeks {
    public void days(int day){
        switch (day){
            case 1:
                System.out.println("Sunday");
                break;

            case 2:
                System.out.println("Monday");
                break;

            case 3:
                System.out.println("Tuesday");
                break;

            case 4:
                System.out.println("Wednesday");
                break;

            case 5:
                System.out.println("Thursday");
                break;

            case 6:
                System.out.println("Friday");
                break;

            case 7:
                System.out.println("Satday");
                break;

            default:
                System.out.println("Invalid Day");
                break;
        }
    }
}
