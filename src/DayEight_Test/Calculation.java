package DayEight_Test;

/**
 * Create a class called Calculation
 * create a method which sums three integer passed via parameter and returns the sums as integer
 * create a method which subtracts the 2 integer passed via parameter
 * create a method which multiply the 2 integers passed via parameter
 * create a method which divides the 2 integers passed via parameter (a divided by b)
 * create a method which show the remainder of 2 integer passed via parameter (a modulus b)
 *
 */
public class Calculation {

    /**
     * a method which sums three integer passed via parameter and returns the sums as integer
     *
     */
    public int intSum(int x, int y, int z){
        int num = x+ y + z;
        return num;
    }

    /**
     * a method which subtracts the 2 integer passed via parameter
     */
    public int intSub(int x, int y){
        int num = 0;
        if(x > y){
            num = x - y;
        }
        if(y < x){
           num = y - x;
        }
        return num;
    }

    /**
     * a method which multiply the 2 integers passed via parameter
     */
    public int intMul(int x, int y){
        int num = x * y;
        return num;
    }

    /**
     * create a method which divides the 2 integers passed via parameter (a divided by b)
     */
    public int intDiv(int a, int b){
        int num = 0;
        if( a > b){
            num = a / b;
        }
        if(b > a){
            num = b / a;
        }
        return num;
    }

    /**
     * a method which show the remainder of 2 integer passed via parameter (a modulus b)
     */
    public int intMod(int a, int b){
        int num = a % b;
        return num;
    }
}
