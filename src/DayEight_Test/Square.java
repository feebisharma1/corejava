package DayEight_Test;

/**
 * This is a child class
 */
public class Square extends Shape {
    @Override
    public void getHeight(int x) {
        super.getHeight(x);
    }

    @Override
    public void getWidth(int y) {
        super.getWidth(y);
    }

    @Override
    public void getArea(int x, int y) {
        int num = x * x;
        System.out.println(num);
    }
}
