package Date;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class SampleDate {
    public void displayDate() {
        Date date = new Date();
        System.out.println(date);
    }

    public void displayCalender(){
        Calendar calender  = Calendar.getInstance();
        System.out.println(calender.getTime());

    }

    public void formatDate(){
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("mm-dd-yyyy");
        System.out.println(format.format(date));
    }
}
