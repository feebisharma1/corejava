package Date;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

class SampleLocalDate {
    public void displayLocalDate(){
        LocalDate localDate = LocalDate.now();
        System.out.println(localDate);
    }

    public void DisplayLocalTime(){
        LocalTime localTime = LocalTime.now();
        System.out.println(localTime);
    }

    public void displayLocalDAteTime(){
        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println(localDateTime);
    }

    public void formatLocalDate(){
        LocalDate localDate = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/YYYY");
        System.out.println(formatter.format(localDate));
    }


}
