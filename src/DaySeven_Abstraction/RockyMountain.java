package DaySeven_Abstraction;

public class RockyMountain extends Land {

    @Override
    public void getHeights() {
        System.out.println(" Heights less than 1000m");
    }

    @Override
    public void getPopulation() {
        System.out.println("Good number of population");
    }

    @Override
    public void getHouses() {
        System.out.println("1000 houses per city");
    }
}
