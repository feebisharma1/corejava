package DaySeven_Abstraction;

import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

public class Plain extends Land {
    @Override
    public void getHeights() {
        System.out.println("No heights at all");
    }

    @Override
    public void getPopulation() {
        System.out.println("Lot of population ");
    }
}
