package DaySeven_Abstraction;

/**
 * First Abstraction child class
 */
public class SnowMountain extends Land {
    @Override
    public void getHeights() {
        System.out.println("Heights are up to 8848m");
    }

    @Override
    public void getPopulation() {
        System.out.println("Very few population");
    }

    @Override
    public void getHouses() {
        super.getHouses();
    }
}
